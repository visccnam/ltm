/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.Serializable;
import java.util.Map;


/**
 *
 * @author User
 */
public class GameDTO implements Serializable{
    private User u;
    private String IPAdress;
    private Map<String,Integer> cuoc;

    public GameDTO() {
    }

    public GameDTO(User u, String IPAdress, Map<String, Integer> cuoc) {
        this.u = u;
        this.IPAdress = IPAdress;
        this.cuoc = cuoc;
    }

    public User getU() {
        return u;
    }

    public void setU(User u) {
        this.u = u;
    }

    public String getIPAdress() {
        return IPAdress;
    }

    public void setIPAdress(String IPAdress) {
        this.IPAdress = IPAdress;
    }

    public Map<String, Integer> getCuoc() {
        return cuoc;
    }

    public void setCuoc(Map<String, Integer> cuoc) {
        this.cuoc = cuoc;
    }

    @Override
    public String toString() {
        return "GameDTO{" + "u=" + u + ", IPAdress=" + IPAdress + ", cuoc=" + cuoc + '}';
    }
    
    
}
