/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import Model.User;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author leequ
 */
public class DAO {
    public  Connection con;
   
    
    public Connection getConnection(){
        Connection connection= null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con=DriverManager.getConnection("jdbc:mysql://localhost:3306/btcc","root","");
        } catch (ClassNotFoundException ex) {
            
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return connection;
        
    }
   public User checkLogin(String username,String password){
       Connection connection= getConnection();
       String sql = "select * from user where username=? and password=?";
        try {
            PreparedStatement preparedStatement= connection.prepareStatement(sql);
            preparedStatement.setString(1, username);
            preparedStatement.setString(1, password);
            ResultSet rs= preparedStatement.executeQuery();
            if(rs.next()){
                User u = new User();
                u.setId(rs.getInt("id"));
                u.setUsername(rs.getString("username"));
                u.setPassword(password);
                u.setMoney(rs.getInt("money"));
                u.setNickname(rs.getString("nickname"));
                return u;
            }
            else return null;
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
       return null;
   }
}
